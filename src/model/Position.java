package model;

/**
 * In Supermarket have position, Director, Seller, Bookkeeper... create ENUM for employee position.
 */
public enum Position {
    DIRECTOR,
    SELLER,  // Vacharox
    BOOKKEEPER; // Hashvapah
}

package model;

/**
 * This class created for regular customers, whoes have a bonus card.
 */
public class Customer {
    private String firstName;
    private String LastName;
    private String bonusID;
    private int bonusDiscount;

    /**
     * Constructor by parameter
     * @param firstName
     * @param lastName
     * @param bonusID
     * @param bonusDiscount
     */
    public Customer(String firstName, String lastName, String bonusID, int bonusDiscount) {
        this.firstName = firstName;
        this.LastName = lastName;
        this.bonusID = bonusID;
        this.bonusDiscount = bonusDiscount;
    }

    /**
     * Constructor by parameter..
     * @param bonusID
     * @param bonusDiscount
     */
    public Customer(String bonusID, int bonusDiscount) {
        this.bonusID = bonusID;
        this.bonusDiscount = bonusDiscount;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getBonusID() {
        return bonusID;
    }

    public void setBonusID(String bonusID) {
        this.bonusID = bonusID;
    }

    public int getBonusDiscount() {
        return bonusDiscount;
    }

    public void setBonusDiscount(int bonusDiscount) {
        this.bonusDiscount = bonusDiscount;
    }
}

package model;

/**
 * Here are the types of products available in the store
 */
public enum ProductType {
    SWEETS,
    COFFEE,
    TEA,
    MEAT,
    ALCOHOLIC,
    BEER


}

package model;

/**
 * Class, which describes the product properties.
 */
public class Product {
    private int id;
    private ProductType type;
    private double price;

    /**
     * Constructor
     * @param id
     */
    public Product(int id){
        this.id = id;
    }

    /**
     * Constructor by parameter.
     * @param id
     * @param type
     * @param price
     */
    public Product(int id, ProductType type, double price) {
        this.id = id;
        this.type = type;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}

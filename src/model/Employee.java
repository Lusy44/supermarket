package model;

/**
 * Abstract class for all people,  who work in supermarket.
 */
public class Employee {
    private int id;
    private String pass;
    private String firstName;
    private String lastName;
    private Position position;
    private double salary;


    /**
     * Short Constructor by parameter.
     * @param id
     * @param position
     */
    public Employee(int id, String pass, Position position) {
        this.id = id;
        this.pass = pass;
        this.position = position;

    }

    /**
     * Constructor which contains all properties by Employee
     * @param firstName
     * @param lastName
     * @param position
     * @param salary
     */
    public Employee(int id, String firstName, String lastName, Position position, double salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.position = position;
        this.salary = salary;

    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }


    @Override
    public String toString() {
        return "model.Employee{" +
                "Id='" + id + '\'' +
                ", position=" + position + '}';
    }
}


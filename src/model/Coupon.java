package model;

import java.time.LocalDate;

/**
 * Describes each coupon that goes through the cash register.
 */
public class Coupon {
    private int couponId;
    private int sellerId;
    private String customerId;
    private Product[] products;
    private LocalDate currentDate;

    /**
     * Constructor by parameter.
     * @param couponId
     * @param sellerId
     * @param customerId
     * @param products
     */
    public Coupon(int couponId, int sellerId, String customerId, Product[] products) {
        this.couponId = couponId;
        this.sellerId = sellerId;
        this.customerId = customerId;
        this.products = products;
        this.currentDate = LocalDate.now();
    }

    public int getCouponId() {
        return couponId;
    }

    public void setCouponId(int couponId) {
        this.couponId = couponId;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Product[] getProducts() {
        return products;
    }

    public void setProducts(Product[] products) {
        this.products = products;
    }

    public LocalDate getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(LocalDate currentDate) {
        this.currentDate = currentDate;
    }
}

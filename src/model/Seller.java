package model;

/**
 * Class, which contains all properties the  seller
 */
public class Seller extends Employee {

    /**
     * Constructor by parameter
     * @param position
     */
    public Seller(int id, String pass, Position position) {
        super(id, pass, position);
    }

}

import model.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Writing program for SuperMarket...
 * Run all project... :)
 */
public class Demo {
     //creating Map for all classic
      static Map<Integer, Seller> sellers = new HashMap<>();
      static Map<Integer, Customer> customers = new HashMap<>();
      static Map<Integer, Product> products = new HashMap<>();
      static Map<Integer, Coupon> coupons = new HashMap<>();

    public static void main(String[] args) {
        initData();
        System.out.println("Count All coupons products = " + getCountOfSoldProduct());
        System.out.println("The general Price = " +  getPriceInAllDay());


    }

    /**
     * Find product by given id
     * @param id
     */
    public static  Product getProductById(int id){
       for(Integer prod: products.keySet()){
           if(id == products.get(prod).getId()){
               return products.get(prod);
           }
       }
     return null;
    }

    /**
     * Find coupon by given id
     * @param id
     * @return
     */
    public static Coupon getCouponById(int id){
        for (Integer currentId: coupons.keySet()){
            if(id == coupons.get(currentId).getCouponId()){
                return coupons.get(currentId);
            }
        }
        return null;
    }

    /**
     * Find customer by given id.
     * @param id
     * @return
     */
    public static Customer getCustomerByID(String id){
        for (Integer customerID: customers.keySet()){
            if(id.equals(customers.get(customerID).getBonusID())){
                return customers.get(customerID);
            }
        }
        return null;
    }

    /**
     * Calculating all profit(vacharq) by given Seller
     * @param selleriId
     * @return
     */
    public static double getProfitBySellerId(int selleriId){
        double profit = 0;

        for(Integer couponId: coupons.keySet()){
            if(selleriId == coupons.get(couponId).getSellerId()){
                profit += getDiscountedPriceByCoupon(couponId);
            }
        }
        return profit;
    }

    /**
     * Calculating the total coupons of sellers in the day.
     * (All day coupons - turnover)
     * @return
     */
    public static double getPriceInAllDay(){
        double allPrice = 0;

        for (Integer sellerId: sellers.keySet()){
            allPrice += getProfitBySellerId(sellerId);
        }
        return allPrice;
    }

    /**
     * Return all count Sold(vacharvac) products.
     * @return
     */
    public static int getCountOfSoldProduct(){
        int count = 0;

        for(Integer id: coupons.keySet()){
            count += coupons.get(id).getProducts().length;
        }
        return count;
    }

    /**
     * Calculating price for customer using bonusDiscount
     * @param couponId
     * @return
     */
    public static double getDiscountedPriceByCoupon(int couponId){
        double currentPrice = getPriceInGivenCoupon(couponId);

        int bonusDiscount = getCustomerByID(getCouponById(couponId).getCustomerId())
                            .getBonusDiscount();

        return currentPrice - (currentPrice * bonusDiscount / 100);

    }

    /**
     * Calculating all product prices in given coupon witout bonus discount.(One coupon)
     * @param couponId
     * @return
     */
    private static double getPriceInGivenCoupon(int couponId){
       double price = 0;
       Coupon coupon = getCouponById(couponId);

       for (Product currentProd: coupon.getProducts()){
           price += getProductById(currentProd.getId()).getPrice();
       }
       return  price;
    }

    /**
     *Counts the day coupons at a discounted price by to the customer ID
     * @param customerId
     * @return
     */
    public static double getPriceByCustomerId(String customerId){
        Customer currentCustomer = getCustomerByID(customerId);
        double price = 0;

        for (Integer couponID: coupons.keySet()){
            if(currentCustomer.getBonusID().equals(coupons.get(couponID).getCustomerId())){
                price += getDiscountedPriceByCoupon(couponID);
            }
        }
        return  price;
    }

    /**
     * Cheching is Seller working
     * @param sellerId
     * @return
     */
    public static void loginSeller(int sellerId){
        Seller seller = sellers.get(sellerId);
        boolean isWorking = isUserValid(seller.getId(), seller.getPass());

        System.out.println(isWorking ? "This Seller working" :
                                       "This user doesn't work..." );
    }

    /**
     * Check is valid seller data valid
     * @param id
     * @param pass
     * @return
     */
    private static boolean isUserValid(int id, String pass){
         Seller seller = sellers.get(id);

          return (id == seller.getId() && pass == seller.getPass());
    }


    /**
     * prints the movement of the whole day
     * HDM for a DIRECTOR  or BOOKKEEPER
     * @return
     */
    public static void printSales(Position position, int sellerId){
        switch(position){
            case BOOKKEEPER:
                System.out.println("Price in all day: " + getPriceInAllDay());
            case DIRECTOR:
                System.out.println("Profit by SellerId: " +getProfitBySellerId(sellerId));
        }
    }



    /**
     * Initialize all data
     */
    private static void initData(){
        initSellers();
        initCustomers();
        initProducts();
        initCoupons();
    }

    /**
     * Add new SELLER object and initialize...
     * @return
     */
    private static void initSellers(){
        Map <String, String> m = new HashMap<>();

         sellers.put(1, new Seller(1, "AA", Position.SELLER));
         sellers.put(2, new Seller(2, "BB", Position.SELLER));
         sellers.put(3, new Seller(3, "CC", Position.SELLER));
         sellers.put(4, new Seller(4, "DD", Position.SELLER));
         sellers.put(5, new Seller(5, "EE", Position.SELLER));
         sellers.put(6, new Seller(6, "FF", Position.SELLER));
    }

    /**
     * Add in customer list new Customer.
     * @return
     */
    private static void initCustomers(){
        customers.put(1, new Customer("A", 10));
        customers.put(2, new Customer("B", 15));
        customers.put(3, new Customer("C", 20));
        customers.put(4, new Customer("D", 50));
    }

    /**
     * Add in product list new product.
     */
    private static void initProducts(){
        products.put(1,new Product(1, ProductType.BEER, 350));
        products.put(2,new Product(2, ProductType.SWEETS, 1500));
        products.put(3, new Product(3, ProductType.COFFEE, 1800));
        products.put(4, new Product(4, ProductType.MEAT, 4500));
        products.put(5, new Product(5, ProductType.SWEETS, 2500));
    }

    /**
     * Create coupons list.
     */
    private static void initCoupons(){
        coupons.put(1, new Coupon(1, 3, "A", new Product[]{products.get(1), products.get(3)}));
        coupons.put(2, new Coupon(2, 1, "C", new Product[]{products.get(3), products.get(4), products.get(5)}));
        coupons.put(3, new Coupon(3, 2, "D", new Product[]{products.get(3), products.get(2), products.get(1)}));
        coupons.put(4, new Coupon(4, 1, "C", new Product[]{products.get(2), products.get(4)}));

    }

}

